package main

import (
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	test_cases "gitlab.com/NikolayZhurbin/go_rest/test-cases"
	"gitlab.com/NikolayZhurbin/go_rest/token"
	user_service "gitlab.com/NikolayZhurbin/go_rest/user-service"
	"log"
)

func main() {

	//client init
	consts.Client = resty.New()
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	var err error

	r := new(token.AccessToken)
	err = r.GetAccessToken(consts.Client, "boa22", "1234")
	if err != nil {
		log.Printf("GetAccessToken - Error: %v", err)
	}
	log.Printf("Token: %v", r.Token)
	//Get User by Auth TokenInfo
	uInfo := new(user_service.UserInfo)
	err = uInfo.GetUserByToken(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	uInfo.PrintUserInfo()

	//test_cases.TestAchievementType(r)
	//test_cases.TestAchievement(r)
	//
	//test_cases.TaskE2EInit(true)
	//test_cases.DistributeNewTaskToNewGroupWithTags(5, true, "[\"elk001\", \"elk002\", \"elk003\"]")

	//test_cases.TestTag(consts.Client, r.Token)

	//fmt.Println(time.Now().Add(-1 * time.Hour*48).Format("2006-01-02 15:04"))
	//fmt.Println(time.Now().Add(time.Hour*48).Format("2006-01-02 15:04"))
	test_cases.TestActivity(consts.Client, r)

	//test_cases.TestGroup(consts.Client, r.Token)
	//test_cases.TestSkill(consts.Client, r.Token)
	//test_cases.TestUser(consts.Client, r.Token)
	//test_cases.TestPriority(consts.Client, r.Token)
	//test_cases.TestStatus(consts.Client, r.Token)
	//test_cases.TestTask(consts.Client, r.Token, uInfo.AccountId)
}
