package consts

const (
	Server01                = "172.28.128.45"
	Server02                = "46.29.165.167"
	Host                    = Server02
	Ssl                     = "http://"
	UserServiceUserUrl      = Ssl + Host + ":8083/user-service/api/v1/user/"
	UserServiceSkillUrl     = Ssl + Host + ":8083/user-service/api/v1/skill/"
	UserServiceSkillConfUrl = Ssl + Host + ":8083/user-service/api/v1/skillConfirm/{skillId}/user/{userId}?single-confirm"
	UserServiceGroupUrl     = Ssl + Host + ":8083/user-service/api/v1/group/"
	RegistrationServiceUrl  = Ssl + Host + ":8081/registration-service/api/v1/registration/"
	PersonalInfoServiceUrl  = Ssl + Host + ":8082/person-service/api/v1/person/"
	//achievement
	AchievementTypeServiceUrl     = Ssl + Host + ":9114/api/v1/type/"
	AchievementServiceUrl         = Ssl + Host + ":9114/api/v1/achievements/"
	AchievementTagServiceUrl      = Ssl + Host + ":9114/api/v1/achievements/{id}/tags"
	AchievementCommentsServiceUrl = Ssl + Host + ":9114/api/v1/achievements/{id}/comments"
	//tags
	TagServiceUrl = Ssl + Host + ":9112/api/v1/tags/"
	//tasks
	PriorityServiceUrl = Ssl + Host + ":9113/api/v1/priority/"
	StatusServiceUrl   = Ssl + Host + ":9113/api/v1/status/"
	TaskServiceUrl     = Ssl + Host + ":9113/api/v1/tasks/"
	TaskTagServiceUrl  = Ssl + Host + ":9113/api/v1/tasks/{id}/tags"
	CommentsServiceUrl = Ssl + Host + ":9113/api/v1/comments"
	//Calendar
	CalendarServiceUrl        = Ssl + Host + ":8093/api/v1/calendar/"
	GetAllActivitiesUrl       = CalendarServiceUrl + "getAllActivities"
	GetTodayActivitiesUrl     = CalendarServiceUrl + "getTodayActivities"
	GetActivitiesForPeriodUrl = CalendarServiceUrl + "getActivitiesForPeriod"
	GetActivityStatusesUrl    = CalendarServiceUrl + "getActivityStatuses"
	CreateActivityUrl         = CalendarServiceUrl + "createActivity"
	DeleteActivityUrl         = CalendarServiceUrl + "deleteActivity"
	UpdateActivityUrl         = CalendarServiceUrl + "updateActivity"
	CheckInActivityUrl        = CalendarServiceUrl + "checkIn"

	PrintHeader = "-----------------------------------------------------------------"
	MsgHeader   = "-"
)
