package task_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
)

type Priority struct {
	Id    string `json:"id,omitempty"`
	Value string `json:"value,omitempty"`
}

func GetAllPriories(c *resty.Client, token string) (*[]Priority, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.PriorityServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAchievementTypeList CheckResponse failed")
	}

	res := make([]Priority, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal Prioritys: %v", err)
		return nil, err
	}
	return &res, nil

}

//Create new priority
func (t *Priority) CreatePriority(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(t.Value).
		Post(consts.PriorityServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateAchievementType CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), t)
	if err != nil {
		log.Printf("Error while unmarshal Achivement Types: %v", err)
		return nil
	}
	return nil
}

//Delete priority by ID
func (t *Priority) DeletePriority(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": t.Id}).
		Delete(consts.PriorityServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteAchievementType CheckResponse failed")
	}
	return nil
}

func (t *Priority) Print() {
	fmt.Println("Printing Priority")
	fmt.Printf("ID: %v\n", t.Id)
	fmt.Printf("Value: %v\n", t.Value)
	fmt.Println("-----------------------------------------------------------------")
}
