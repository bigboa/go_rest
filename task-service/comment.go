package task_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"gitlab.com/NikolayZhurbin/go_rest/tag-service"
	"log"
)

type Comment struct {
	Id         string `json:"id,omitempty"`
	AssignedTo string `json:"assignedTo,omitempty"`
	Attachment string `json:"attachment,omitempty"`
	DueDate    string `json:"dueDate,omitempty"` //(format: yyyy-MM-dd)
	Priority   `json:"priority,omitempty"`
	Status     `json:"status,omitempty"`
	Subject    string            `json:"subject,omitempty"`
	Tags       []tag_service.Tag `json:"tags,omitempty"`
	TaskId     string            `json:"taskid,omitempty"`
	Text       string            `json:"text,omitempty"`
}

//Get all task comments
//?taskId=9de710e2-0ac1-4ca2-8cb2-e7c4e70227d3
func GetAllCommentsByTaskId(c *resty.Client, token, tid string) (*[]Comment, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"taskId": tid}).
		Get(consts.CommentsServiceUrl + "?taskId={taskId}")

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAllStatuses CheckResponse failed")
	}

	res := make([]Comment, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal Status: %v", err)
		return nil, err
	}
	return &res, nil
}

//Create comment
func (comm *Comment) CreateComment(token string) error {
	formData := []helper.FormData{
		{Key: "subject", Value: comm.Subject},
		{Key: "statusId", Value: comm.Status.Id},
		{Key: "priorityId", Value: comm.Priority.Id},
		{Key: "text", Value: comm.Text},
		{Key: "assignedTo", Value: comm.AssignedTo},
		{Key: "dueDate", Value: comm.DueDate},
	}
	url := consts.CommentsServiceUrl
	resp, err := helper.DoPostFormData(url, &formData, token)

	b := make([]byte, 40)
	resp.Body.Read(b)
	comm.Id = string(b)

	if !helper.CheckRawResponse(resp, err) {
		return errors.New("CreateComment CheckResponse failed")
	}

	return nil
}

func (comm *Comment) ReplyToComment(token, cId, tId string) error {
	formData := []helper.FormData{
		{Key: "subject", Value: comm.Subject},
		{Key: "statusId", Value: comm.Status.Id},
		{Key: "priorityId", Value: comm.Priority.Id},
		{Key: "text", Value: comm.Text},
		{Key: "assignedTo", Value: comm.AssignedTo},
		{Key: "dueDate", Value: comm.DueDate},
		{Key: "replyTo", Value: cId},
		{Key: "taskId", Value: tId},
	}
	url := consts.CommentsServiceUrl
	resp, err := helper.DoPostFormData(url, &formData, token)

	b := make([]byte, 40)
	resp.Body.Read(b)
	comm.Id = string(b)

	if !helper.CheckRawResponse(resp, err) {
		return errors.New("CreateComment CheckResponse failed")
	}

	return nil
}

func (comm *Comment) PrintComment() {
	fmt.Println("Printing Comment")
	fmt.Printf("CommentId: %v\n", comm.Id)
	fmt.Printf("AssignedTo: %v\n", comm.AssignedTo)
	fmt.Printf("Attachment: %v\n", comm.Attachment)
	fmt.Printf("DueDate: %v\n", comm.DueDate)
	fmt.Printf("Priority: %v\n", comm.Priority.Value)
	fmt.Printf("Status: %v\n", comm.Status.Value)
	fmt.Printf("Subject: %v\n", comm.Subject)
	fmt.Printf("TaskId: %v\n", comm.TaskId)
	fmt.Printf("Text: %v\n", comm.Text)
	fmt.Println("-------------------------------------------------------------------------------------------------")
}
