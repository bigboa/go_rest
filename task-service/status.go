package task_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
)

type Status struct {
	Id    string `json:"id,omitempty"`
	Value string `json:"value,omitempty"`
}

//Get all statuses
func GetAllStatuses(c *resty.Client, token string) (*[]Status, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.StatusServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAllStatuses CheckResponse failed")
	}

	res := make([]Status, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal Status: %v", err)
		return nil, err
	}
	return &res, nil

}

//Create new status
func (t *Status) CreateStatus(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(t.Value).
		Post(consts.StatusServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateStatus CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), t)
	if err != nil {
		log.Printf("Error while unmarshal Status: %v", err)
		return nil
	}
	return nil
}

//Delete status by ID
func (t *Status) DeleteStatus(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": t.Id}).
		Delete(consts.StatusServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteStatus CheckResponse failed")
	}
	return nil
}

func (t *Status) Print() {
	fmt.Println("Printing Status")
	fmt.Printf("ID: %v\n", t.Id)
	fmt.Printf("Value: %v\n", t.Value)
	fmt.Println("-----------------------------------------------------------------")
}
