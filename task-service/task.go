package task_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	tag_service "gitlab.com/NikolayZhurbin/go_rest/tag-service"
	"log"
)

type Parent struct {
	Id     string `json:"id"`
	Parent string `json:"parent"`
}

type Task struct {
	Id          string            `json:"id,omitempty"`
	CreatedDate string            `json:"createdDate,omitempty"`
	Tags        []tag_service.Tag `json:"tags,omitempty"`
	ParentId    string            `json:"parentId,omitempty"`
}

type distributeTask struct {
	AssignTo []string `json:"assignTo"`
	TaskId   string   `json:"taskId"`
}

func GetAllTasks(c *resty.Client, token string) (*[]Task, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.TaskServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAllTasks CheckResponse failed")
	}

	//fmt.Println("---GetAllTasks body resp:")
	//fmt.Println(string(resp.Body()))
	res := make([]Task, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal GetAllTasks: %v", err)
		return nil, err
	}
	return &res, nil

}

func (t *Task) DeleteTask(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": t.Id}).
		Delete(consts.TaskServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteTask CheckResponse failed")
	}
	return nil
}

func (t *Task) TagTask(c *resty.Client, token, tags string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": t.Id}).
		SetBody(tags).
		Post(consts.TaskTagServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("TagTask CheckResponse failed")
	}
	return nil
}

func (t *Task) DistributeTask(c *resty.Client, token string, assignTo *[]string, ptr bool) error {
	if ptr {
		fmt.Println("-Distributing Task")
	}
	d := distributeTask{
		TaskId:   t.Id,
		AssignTo: *assignTo,
	}
	body, err := json.Marshal(d)
	if err != nil {
		log.Printf("Error during Marshal: %v", err)
		return err
	}
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		Post(consts.TaskServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("DistributeTask CheckResponse failed")
	}

	return nil
}

func (t *Task) Print() {
	fmt.Println("Printing Tasks")
	fmt.Printf("ID: %v\n", t.Id)
	fmt.Printf("Parent ID: %v\n", t.ParentId)
	fmt.Printf("CreatedDate: %v\n", t.CreatedDate)
	fmt.Printf("Tags: %v\n", t.Tags)
	fmt.Println(consts.PrintHeader)
}
