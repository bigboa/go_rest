package token

import (
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty"
	"log"
)

type AccessToken struct {
	Token string `json:"access_token"`
	ExpiresIn int `json:"expires_in"`
	RefreshExpiresIn int `json:"refresh_expires_in"`
	RefreshToken string `json:"refresh_token"`
	TokenType string `json:"token_type"`
	NotBeforePolicy int `json:"not-before-policy"`
	SessionState string `json:"session_state"`
	Scope string `json:"scope"`
}

func (r *AccessToken) GetAccessToken (c *resty.Client, user, password string) error {
	resp, err := c.R().
		SetBody(`username=`+user+`&password=`+password+`&client_id=ui-service&grant_type=password&client_secret=7354bbd7-9fc4-4693-a9af-43a29c731bae`).
		SetHeader("Content-Type", "application/x-www-form-urlencoded").
		Post("http://46.17.45.170:8080/auth/realms/university-project/protocol/openid-connect/token")

	if err != nil {
		log.Printf("Error while gettign the token: %v", err)
		return err
	}

	err = json.Unmarshal(resp.Body(), r)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return err
	}
	return nil
}

func (r *AccessToken) PrintAccessToken() {
	fmt.Println("Printing Access Token:")
	fmt.Printf("Token: %v\n", r.Token)
	fmt.Printf("ExpiresIn: %v\n", r.ExpiresIn)
	fmt.Printf("RefreshExpiresIn: %v\n", r.RefreshExpiresIn)
	fmt.Printf("RefreshToken: %v\n", r.RefreshToken)
	fmt.Printf("TokenType: %v\n", r.TokenType)
	fmt.Printf("NotBeforePolicy: %v\n", r.NotBeforePolicy)
	fmt.Printf("SessionState: %v\n", r.SessionState)
	fmt.Printf("Scope: %v\n", r.Scope)
	fmt.Println("-------------------------------------------------------------------------------------------------")
}