package helper

import (
	"github.com/go-resty/resty"
	"log"
	"net/http"
)

func CheckResponse(resp *resty.Response, err error) bool {
	if err != nil {
		log.Printf("Error from request: %v", err)
		return false
	}
	if resp.StatusCode() == 404 {
		log.Printf("Request: %v\n", *resp.Request)
		log.Printf("Responce: %v\n", resp)
		log.Println("Requested object is Not Found")
		return true
	}
	if resp.StatusCode() == 201 {
		log.Println("Requested object is Created")
		return true
	}
	if resp.StatusCode() == 204 {
		log.Println("Requested object is Deleted")
		return true
	}
	if resp.StatusCode() != 200 {
		log.Printf("Status code: %v", resp.StatusCode())
		log.Printf("Request Header: %v\n", resp.Request.Header)
		log.Printf("Request Body: %v\n", resp.Request)
		log.Printf("Responce: %v\n", resp)
		return false
	}
	return true
}

func CheckRawResponse(resp *http.Response, err error) bool {
	if err != nil {
		log.Printf("Error from request: %v", err)
		return false
	}
	if resp.StatusCode == 404 {
		log.Printf("Request: %v\n", *resp.Request)
		log.Printf("Response: %v\n", resp)
		log.Println("Requested object is Not Found")
		return true
	}
	if resp.StatusCode == 201 {
		log.Println("Requested object is Created")
		return true
	}
	if resp.StatusCode == 204 {
		log.Println("Requested object is Deleted")
		return true
	}
	if resp.StatusCode != 200 {
		log.Printf("Status code: %v", resp.Status)
		log.Printf("Request: %v\n", resp.Request)
		log.Printf("Response: %v\n", resp)

		body := make([]byte, 1000000)
		n, _ := resp.Body.Read(body)
		body = body[0:n]
		log.Printf("Responce body: %v\n", string(body))
		return false
	}
	return true
}
