package achievement_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
)

type AchievementType struct {
	Id    string `json:"id,omitempty"`
	Value string `json:"value,omitempty"`
}

func GetAchievementTypeList(c *resty.Client, token string) (*[]AchievementType, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.AchievementTypeServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAchievementTypeList CheckResponse failed")
	}

	res := make([]AchievementType, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal Achievement Types: %v", err)
		return nil, err
	}
	return &res, nil
}

func (a *AchievementType) Print() {
	fmt.Println("Printing AchievementType")
	fmt.Printf("ID: %v\n", a.Id)
	fmt.Printf("Value: %v\n", a.Value)
	fmt.Println("-----------------------------------------------------------------")
}

func (a *AchievementType) CreateAchievementType(c *resty.Client, token string) error {
	//body, err := json.Marshal(*a)
	//if err != nil {
	//	log.Printf("Error while Marshal Achievement Type: %v", err)
	//	return nil
	//}
	body := "\"" + a.Value + "\""

	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		Post(consts.AchievementTypeServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateAchievementType CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), a)
	if err != nil {
		log.Printf("Error while unmarshal Achievement Types: %v", err)
		return nil
	}
	return nil
}

func (a *AchievementType) DeleteAchievementType(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": a.Id}).
		Delete(consts.AchievementTypeServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteAchievementType CheckResponse failed")
	}
	return nil
}
