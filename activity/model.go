package activity

import tag_service "gitlab.com/NikolayZhurbin/go_rest/tag-service"

type Duration struct {
	Weeks   int `json:"weeks,omitempty"`
	Days    int `json:"days,omitempty"`
	Hours   int `json:"hours,omitempty"`
	Minutes int `json:"minutes,omitempty"`
	Seconds int `json:"seconds,omitempty"`
}

type RepeatInterval struct {
	Weeks   int `json:"weeks,omitempty"`
	Days    int `json:"days,omitempty"`
	Hours   int `json:"hours,omitempty"`
	Minutes int `json:"minutes,omitempty"`
	Seconds int `json:"seconds,omitempty"`
}

type Participant struct {
	Id        string `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	AccountId string `json:"accountId,omitempty"`
	GroupId   string `json:"groupId,omitempty"`
	Status    string `json:"status,omitempty"`
}

type Schedule struct {
	Id                string        `json:"id,omitempty"`
	Description       string        `json:"description,omitempty"`
	StartTime         string        `json:"startTime,omitempty"`
	EndTime           string        `json:"endTime,omitempty"`
	OldTime           string        `json:"oldTime,omitempty"`
	Cancelled         bool          `json:"cancelled,omitempty"`
	Updated           bool          `json:"updated,omitempty"`
	CurrentUserStatus string        `json:"currentUserStatus,omitempty"`
	Participants      []Participant `json:"participants,omitempty"`
}

type Activity struct {
	Id                string `json:"id,omitempty"`
	Subject           string `json:"subject,omitempty"`
	Description       string `json:"description,omitempty"`
	ActivityTime      string `json:"activityTime,omitempty"`
	Duration          `json:"duration,omitempty"`
	Repeated          bool `json:"repeated,omitempty"`
	RepeatInterval    `json:"repeatInterval,omitempty"`
	StartDate         string           `json:"startDate,omitempty"`
	EndDate           string           `json:"endDate,omitempty"`
	CreatedById       string           `json:"createdById,omitempty"`
	CreatedByName     string           `json:"createdByName,omitempty"`
	Cancelled         bool             `json:"cancelled,omitempty"`
	Updated           bool             `json:"updated,omitempty"`
	CurrentUserStatus string           `json:"currentUserStatus,omitempty"`
	Attachment        string           `json:"attachment,omitempty"`
	Participants      []Participant    `json:"participants,omitempty"`
	Schedules         []Schedule       `json:"schedules,omitempty"`
	Tags              tag_service.Tags `json:"tags,omitempty"`
}

type ActivitiesForPeriod struct {
	StartDate string `json:"startDate,omitempty"`
	EndDate   string `json:"endDate,omitempty"`
}

type ActivityStatuses struct {
	Id     string `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Status string `json:"status,omitempty"`
}
