package activity

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
	"strconv"
)

//!
func GetAllActivities(c *resty.Client, token string) (*[]Activity, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.GetAllActivitiesUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAllActivities CheckResponse failed")
	}

	res := make([]Activity, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal GetAllActivities: %v", err)
		return nil, err
	}
	return &res, nil
}

//!
func PrintAllActivities(a *[]Activity, withSchedule bool) {
	for _, v := range *a {
		v.PrintActivity(withSchedule)
	}
}

//!
func (a *Activity) PrintActivity(withSchedule bool) {
	fmt.Println("Printing Activity")
	fmt.Printf("ID: %v\n", a.Id)
	fmt.Printf("Subject: %v\n", a.Subject)
	fmt.Printf("Description: %v\n", a.Description)
	fmt.Printf("ActivityTime: %v\n", a.ActivityTime)
	fmt.Printf("Cancelled: %v\n", a.Cancelled)
	fmt.Printf("CreatedById: %v\n", a.CreatedById)
	fmt.Printf("CreatedByName: %v\n", a.CreatedByName)
	fmt.Printf("CurrentUserStatus: %v\n", a.CurrentUserStatus)
	fmt.Printf("StartDate: %v\n", a.StartDate)
	fmt.Printf("EndDate: %v\n", a.EndDate)
	fmt.Printf("Repeated: %v\n", a.Repeated)
	fmt.Printf("Updated: %v\n", a.Updated)
	fmt.Printf("Tags: %v\n", a.Tags)
	fmt.Printf("Participants: %v\n", a.Participants)
	if withSchedule {
		for _, v := range a.Schedules {
			v.PrintSchedule()
		}

	}
	fmt.Println(consts.PrintHeader)
}

func (s *Schedule) PrintSchedule() {
	fmt.Println("Printing Schedule")
	fmt.Printf("ID: %v\n", s.Id)
	fmt.Printf("Description: %v\n", s.Description)
	fmt.Printf("CurrentUserStatus: %v\n", s.CurrentUserStatus)
	fmt.Printf("StartTime: %v\n", s.StartTime)
	fmt.Printf("EndTime: %v\n", s.EndTime)
	fmt.Printf("OldTime: %v\n", s.OldTime)
	fmt.Printf("Updated: %v\n", s.Updated)
	fmt.Printf("Cancelled: %v\n", s.Cancelled)
	fmt.Printf("Participants: %v\n", s.Participants)
	fmt.Println(consts.PrintHeader)
}

//!
func (a *Activity) CreateActivity(c *resty.Client, token string) error {
	formData := []helper.FormData{
		{Key: "activityTime", Value: a.ActivityTime},
		{Key: "description", Value: a.Description},
		{Key: "duration.hours", Value: strconv.Itoa(a.Duration.Hours)},
		{Key: "subject", Value: a.Subject},
		{Key: "repeated", Value: strconv.FormatBool(a.Repeated)},
		{Key: "repeatInterval.days", Value: strconv.Itoa(a.RepeatInterval.Days)},
	}
	if len(a.Participants) > 0 {
		if len(a.Participants[0].AccountId) > 0 {
			p := helper.FormData{
				Key:   "participants[0].accountId",
				Value: a.Participants[0].AccountId,
			}
			formData = append(formData, p)
		}
		if len(a.Participants[0].GroupId) > 0 {
			p := helper.FormData{
				Key:   "participants[0].groupId",
				Value: a.Participants[0].GroupId,
			}
			formData = append(formData, p)
		}
	}

	url := consts.CreateActivityUrl
	resp, err := helper.DoPostFormData(url, &formData, token)
	if !helper.CheckRawResponse(resp, err) {
		return errors.New("CreateActivity CheckResponse failed")
	}
	byteNum := int64(1000000)
	if resp.ContentLength > 0 {
		byteNum = resp.ContentLength
	}
	b := make([]byte, byteNum)

	n, err := resp.Body.Read(b)
	if err != nil {
		log.Printf("Error while read boby: %v", err)
		return err
	}
	if resp.ContentLength == -1 {
		b = b[0:n]
	}
	err = json.Unmarshal(b, a)
	if err != nil {
		log.Printf("Error while unmarshal CreateActivity: %v", err)
		return err
	}
	return nil
}

//!
func GetActivitiesForPeriod(c *resty.Client, token, start, end string) (a *[]Activity, err error) {
	bodyReq, err := json.Marshal(ActivitiesForPeriod{
		StartDate: start,
		EndDate:   end,
	})
	if err != nil {
		log.Printf("Error while Marshal: %v", err)
		return nil, err
	}
	headers := map[string]string{
		"Authorization": "Bearer " + token,
		"Content-Type":  "application/json",
	}
	body, err := helper.HttpRequest(headers, bodyReq, consts.GetActivitiesForPeriodUrl, "GET")

	if err != nil {
		return nil, errors.New("GetActivitiesForPeriod CheckResponse failed")
	}

	res := make([]Activity, 0)

	//fmt.Println(string(body))

	err = json.Unmarshal(body, &res)
	if err != nil {
		log.Printf("Error while unmarshal GetActivitiesForPeriod: %v", err)
		return nil, err
	}
	return &res, nil
}

//!
func GetTodayActivities(c *resty.Client, token string) (*[]Activity, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.GetTodayActivitiesUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetTodayActivities CheckResponse failed")
	}

	res := make([]Activity, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal GetTodayActivities: %v", err)
		return nil, err
	}
	return &res, nil
}

//!
func (a *Activity) TagActivity(c *resty.Client, token, tags string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(tags).
		SetPathParams(map[string]string{"id": a.Id}).
		Post(consts.CalendarServiceUrl + "{id}" + "/tags")

	if !helper.CheckResponse(resp, err) {
		return errors.New("TagActivity CheckResponse failed")
	}
	return nil
}

//!
func (a *Activity) DeleteActivity(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetQueryParam("activityId", a.Id).
		Delete(consts.DeleteActivityUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteActivity CheckResponse failed")
	}
	return nil
}

//???
func (a *Activity) UpdateActivity(c *resty.Client, token string) error {
	formData := []helper.FormData{
		{Key: "description", Value: a.Description},
		{Key: "subject", Value: a.Subject},
		{Key: "id", Value: a.Id},
	}

	url := consts.UpdateActivityUrl
	resp, err := helper.DoPostFormData(url, &formData, token)
	if !helper.CheckRawResponse(resp, err) {
		return errors.New("CreateActivity CheckResponse failed")
	}
	byteNum := int64(1000000)
	if resp.ContentLength > 0 {
		byteNum = resp.ContentLength
	}
	b := make([]byte, byteNum)

	n, err := resp.Body.Read(b)
	if err != nil {
		log.Printf("Error while read boby: %v", err)
		return err
	}
	if resp.ContentLength == -1 {
		b = b[0:n]
	}
	err = json.Unmarshal(b, a)
	if err != nil {
		log.Printf("Error while unmarshal CreateActivity: %v", err)
		return err
	}
	return nil
}

//!!!
func (a *Activity) ActivityCheckIn(c *resty.Client, token string) error {
	rawBody := struct {
		Status string `json:"currentUserStatus"`
		Id     string `json:"id"`
	}{
		Status: "ACCEPTED",
		Id:     a.Id,
	}
	body, err := json.Marshal(rawBody)
	if err != nil {
		log.Printf("Error while Marshal: %v", err)
		return err
	}
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		Put(consts.CheckInActivityUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("ActivityCheckIn CheckResponse failed")
	}
	return nil
}

func (a *Activity) ActivityStatuses(c *resty.Client, token string) (*[]ActivityStatuses, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetQueryParam("id", a.Id).
		Get(consts.GetActivityStatusesUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("ActivityStatuses CheckResponse failed")
	}

	res := make([]ActivityStatuses, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal ActivityStatuses: %v", err)
		return nil, err
	}
	return &res, nil
}

func (a *ActivityStatuses) Print() {
	fmt.Println("Printing Activity Statuses")
	fmt.Printf("ID: %v\n", a.Id)
	fmt.Printf("Name: %v\n", a.Name)
	fmt.Printf("Status: %v\n", a.Status)

	fmt.Println(consts.PrintHeader)
}
