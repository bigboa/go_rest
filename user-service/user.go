package user_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	tag_service "gitlab.com/NikolayZhurbin/go_rest/tag-service"
	"log"
)

type User struct {
	AccountId string           `json:"accountId,omitempty"`
	UserName  string           `json:"userName,omitempty"`
	Tags      tag_service.Tags `json:"tags,omitempty"`
}

type UserInfo struct {
	User
	Password string `json:"password,omitempty"`
}

//!Get User by Auth TokenInfo
func (u *UserInfo) GetUserByToken(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.UserServiceUserUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetUserByToken CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), u)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return err
	}

	return nil
}

//!Update User by Auth TokenInfo
func (u *UserInfo) UpdateUserByToken(c *resty.Client, token string) error {
	body, err := json.Marshal(*u)
	if err != nil {
		log.Printf("Can't Marshal: %v\n", *u)
		return err
	}

	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		Put(consts.UserServiceUserUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("UpdateUserByToken CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), u)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return err
	}

	return nil
}

//!Get User by accountId
func (u *UserInfo) GetUserByAccountId(c *resty.Client, token, uid string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"accountId": uid}).
		Get(consts.UserServiceUserUrl + "{accountId}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetUserByAccountId CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), u)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return err
	}

	return nil
}

//!Update User by accountId
func (u *UserInfo) UpdateUserByAccountId(c *resty.Client, token, uid string) error {
	body, err := json.Marshal(*u)
	if err != nil {
		log.Printf("Can't Marshal: %v\n", *u)
		return err
	}

	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		SetPathParams(map[string]string{"accountId": uid}).
		Put(consts.UserServiceUserUrl + "{accountId}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("UpdateUserByAccountId CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), u)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return err
	}

	return nil
}

//!Add Group to user
func (u *UserInfo) AddGroupsToUserByAccountId(c *resty.Client, token, uid, gid string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"accountId": uid, "groups": gid}).
		Put(consts.UserServiceUserUrl + "{accountId}/group/{groups}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("AddGroupsToUserByAccountId CheckResponse failed")
	}
	return nil
}

//!Add Group to user by Auth Token Info
func (u *UserInfo) AddGroupsToUserByToken(c *resty.Client, token, gid string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"groups": gid}).
		Put(consts.UserServiceUserUrl + "group/{groups}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("AddGroupsToUserByToken CheckResponse failed")
	}
	return nil
}

//!Get Groups for user by AccountId
func (u *UserInfo) GetGroupsByAccountId(c *resty.Client, token, uid string) (*[]GroupInfo, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"accountId": uid}).
		Get(consts.UserServiceUserUrl + "{accountId}/groups")

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetGroupsByAccountId CheckResponse failed")
	}
	g := make([]GroupInfo, 0)
	err = json.Unmarshal(resp.Body(), &g)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return nil, err
	}

	return &g, nil
}

//!Get Groups for user by Auth TokenInfo
func (u *UserInfo) GetGroupsByToken(c *resty.Client, token string) (*[]GroupInfo, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.UserServiceUserUrl + "groups")

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetGroupsByToken CheckResponse failed")
	}
	g := make([]GroupInfo, 0)
	err = json.Unmarshal(resp.Body(), &g)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return nil, err
	}

	return &g, nil
}

//!Delete user from Group
func (u *UserInfo) DeleteUserFromGroupById(c *resty.Client, token, gid string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": gid}).
		Delete(consts.UserServiceUserUrl + "group/{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteUserFromGroupById CheckResponse failed")
	}
	return nil
}

//!Add Skills to user By AccountId
func (u *UserInfo) AddSkillsToUserByAccountId(c *resty.Client, token, uid, sid string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"accountId": uid, "skills": sid}).
		Put(consts.UserServiceUserUrl + "{accountId}/skill/{skills}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("AddSkillsToUserByAccountId CheckResponse failed")
	}
	return nil
}

//!Add Skills to user by Auth Token Info
func (u *UserInfo) AddSkillsToUserByToken(c *resty.Client, token, sid string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"accountId": u.AccountId, "skills": sid}).
		Put(consts.UserServiceUserUrl + "{accountId}/skill/{skills}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("AddSkillsToUserByAccountId CheckResponse failed")
	}
	return nil
}

//Delete UserSkill ByAccountId
func (u *UserInfo) DeleteUserSkillByAccountId(c *resty.Client, token, uid, sid string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"accountId": uid, "skills": sid}).
		Delete(consts.UserServiceUserUrl + "{accountId}/skill/{skills}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteUserSkillByAccountId CheckResponse failed")
	}
	return nil
}

//Delete UserSkill by Auth Token Info
func (u *UserInfo) DeleteUserSkillByToken(c *resty.Client, token, sid string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"skill": sid}).
		Delete(consts.UserServiceUserUrl + "skill/{skill}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteUserSkillByToken CheckResponse failed")
	}
	return nil
}

//!Get UsersSkills by Auth Token Info
func (u *UserInfo) GetSkillsByToken(c *resty.Client, token string) (*[]UserSkills, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.UserServiceUserUrl + "skill")

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetSkillsByToken CheckResponse failed")
	}
	g := make([]UserSkills, 0)
	err = json.Unmarshal(resp.Body(), &g)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return nil, err
	}

	return &g, nil
}

//!confirmSkill
func (u *UserInfo) ConfirmSkill(c *resty.Client, token, sid, uid string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"skillId": sid, "userId": uid}).
		Put(consts.UserServiceSkillConfUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("PutSkill CheckResponse failed")
	}
	return nil
}

//tag user by token
func (u *UserInfo) TagUser(c *resty.Client, token, tags string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(tags).
		Post(consts.UserServiceUserUrl + "tags")

	if !helper.CheckResponse(resp, err) {
		return errors.New("TagUser CheckResponse failed")
	}
	return nil
}

//tag user by token
func (u *UserInfo) TagUserByAccId(c *resty.Client, token, tags, aid string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(tags).
		SetPathParams(map[string]string{"accountId": aid}).
		Post(consts.UserServiceUserUrl + "{accountId}" + "/tags")

	if !helper.CheckResponse(resp, err) {
		return errors.New("TagUser CheckResponse failed")
	}
	return nil
}

func (u *UserInfo) PrintUserInfo() {
	fmt.Println("Printing User Info")
	fmt.Printf("AccountId: %v\n", u.AccountId)
	fmt.Printf("UserName: %v\n", u.UserName)
	fmt.Printf("Password: %v\n", u.Password)
	fmt.Printf("Tags: %v\n", u.Tags)
	fmt.Println("-------------------------------------------------------------------------------------------------")
}

func (u *User) PrintUser() {
	fmt.Println("Printing User Info")
	fmt.Printf("AccountId: %v\n", u.AccountId)
	fmt.Printf("UserName: %v\n", u.UserName)
	fmt.Println("-------------------------------------------------------------------------------------------------")
}

func (u *UserSkills) PrintUserSkills() {
	fmt.Println("Printing User Skills")
	fmt.Printf("Id: %v\n", u.AccountId)
	fmt.Printf("UserName: %v\n", u.UserName)
	fmt.Printf("SkillID: %v\n", u.Skill.Id)
	fmt.Printf("SkillName: %v\n", u.Value)
	fmt.Printf("Count: %v\n", u.Count)
	fmt.Println("Confirmations:")
	conf := u.Confirmations
	for _, c := range conf {
		c.PrintUser()
	}
	fmt.Println("-------------------------------------------------------------------------------------------------")
}
