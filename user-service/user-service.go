package user_service

import (
	"fmt"
	"gitlab.com/NikolayZhurbin/go_rest/achievement-service"
	"gitlab.com/NikolayZhurbin/go_rest/activity"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/registration-service"
	"gitlab.com/NikolayZhurbin/go_rest/token"
	"log"
	"math/rand"
	"strconv"
	"time"
)

func CreateUser(Email, FirstName, LastName, MiddleName, Phone, UserName, Password string) error {
	u := registration_service.CreateSelfRegistration(Email, FirstName, LastName, MiddleName, Phone, UserName, Password)
	err := u.CreateUser(consts.Client)
	if err != nil {
		log.Printf("Error - CreateUser: %v", err)
		return err
	}
	return nil
}

func GetAccessToken(UserName, Password string) (*token.AccessToken, error) {
	r := new(token.AccessToken)
	err := r.GetAccessToken(consts.Client, UserName, Password)
	if err != nil {
		log.Printf("GetAccessToken - Error: %v", err)
		return nil, err
	}
	return r, nil
}

func CreateUserAndGetToken(Email, FirstName, LastName, MiddleName, Phone, UserName, Password string) (t *token.AccessToken, err error) {
	err = CreateUser(Email, FirstName, LastName, MiddleName, Phone, UserName, Password)
	if err != nil {
		return nil, err
	}
	t, err = GetAccessToken(UserName, Password)
	if err != nil {
		return nil, err
	}
	return t, nil
}

func CreateNUsersWithSkillsInOneGroup(t *token.AccessToken, n int, prt bool, tags string) string {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())
	//gInfo := new(user_service.GroupInfo)
	gInfo := CreateGroupWithTags(t, num, tags, prt)
	act := CreateActivityWithTagToGroup(t, tags, gInfo.Id, time.Now().Format("2006-01-02 15:04"))

	for i := 0; i < n; i++ {
		if prt {
			fmt.Printf("-Creating User: %v\n", i+1)
		}
		num = strconv.Itoa(rNum.Int())
		t, err := CreateUserAndGetToken(
			"test"+num+"@test.ru",
			"First"+num+"Name",
			"Last"+num+"Name",
			"Middle"+num+"Name",
			"1234",
			"test"+num+"User",
			"1234",
		)
		if err != nil {
			log.Fatalln(err)
		}
		if prt {
			fmt.Printf("---Access Token: %v\n", *t)
		}
		uInfo := new(UserInfo)
		err = uInfo.GetUserByToken(consts.Client, t.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}

		err = uInfo.TagUser(consts.Client, t.Token, tags)
		if err != nil {
			log.Fatalf("TagUser Error: %v", err)
		}
		s := CreateSkill("Skill_" + num)
		err = s.PostSkill(consts.Client, t.Token)
		if err != nil {
			log.Fatalf("CreateSkill Error: %v", err)
		}
		err = s.TagSkill(consts.Client, t.Token, tags)
		if err != nil {
			log.Fatalf("TagSkill Error: %v", err)
		}
		err = uInfo.AddSkillsToUserByToken(consts.Client, t.Token, s.Id)
		if err != nil {
			log.Fatalf("AddSkillsToUserByAccountId Error: %v", err)
		}
		if prt {
			fmt.Printf("---User Info: %v\n", *uInfo)
		}
		err = uInfo.AddGroupsToUserByAccountId(consts.Client, t.Token, uInfo.AccountId, gInfo.Id)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		if prt {
			fmt.Printf("---User: %v; added to Group: %v\n", uInfo.AccountId, gInfo.Id)
		}
		ach := CreateAchievement(t, num, tags, "2019-12-31", false)
		if prt {
			fmt.Printf("---Achievement Created: %v\n", ach.Id)
		}
		err = act.ActivityCheckIn(consts.Client, t.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		if prt {
			fmt.Printf("---User: %v; checkIn in activity: %v\n", uInfo.AccountId, act.Id)
		}
	}
	return gInfo.Id
}

func CreateGroupWithTags(t *token.AccessToken, num, tags string, prt bool) *GroupInfo {
	gInfo := new(GroupInfo)
	gInfo = CreateGroupInfo(false, "Task Group "+num, "Task Group "+num, "Task Group "+num)
	err := gInfo.CreateGroup(consts.Client, t.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	if prt {
		fmt.Printf("---Group Info: %v\n", *gInfo)
	}
	err = gInfo.TagGroup(consts.Client, t.Token, tags)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	return gInfo
}

func CreateAchievement(t *token.AccessToken, num, tags, date string, prt bool) *achievement_service.Achievement {
	ach := achievement_service.Achievement{
		Name:              "Achievement_" + num,
		ParticipantsCount: 170,
		Description:       "Description" + num,
		Date:              date,
	}
	err := ach.CreateAchievement(t.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	err = ach.TagAchievement(consts.Client, t.Token, tags)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	if prt {
		ach.Print()
	}
	return &ach
}

func CreateActivityWithTagToGroup(t *token.AccessToken, tags, gId, aTime string) *activity.Activity {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())
	newAct := activity.Activity{
		Subject:      "CreateActivityWithTagToGroup_" + num,
		Description:  "CreateActivityWithTagToGroup_" + num,
		ActivityTime: aTime,
		Duration: activity.Duration{
			Hours: 1,
		},
		Repeated: true,
		RepeatInterval: activity.RepeatInterval{
			Days: 28,
		},
		Participants: []activity.Participant{
			{GroupId: gId},
		},
	}
	err := newAct.CreateActivity(consts.Client, t.Token)
	if err != nil {
		log.Fatal(err)
	}

	err = newAct.TagActivity(consts.Client, t.Token, tags)
	if err != nil {
		log.Fatal(err)
	}
	return &newAct
}
