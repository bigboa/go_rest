package user_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	tag_service "gitlab.com/NikolayZhurbin/go_rest/tag-service"
	"log"
)

type ConfirmBy struct {
	Id   string `json:"id,omitempty"`
	User `json:"confirmBy,omitempty"`
}

type UserSkills struct {
	Id            string `json:"id,omitempty"`
	User          `json:"user,omitempty"`
	Skill         `json:"skill,omitempty"`
	Confirmations []ConfirmBy `json:"confirmations,omitempty"`
	Count         int         `json:"count,omitempty"`
}

type Skill struct {
	Id    string           `json:"id,omitempty"`
	Value string           `json:"value,omitempty"`
	Tags  tag_service.Tags `json:"tags,omitempty"`
}

func CreateSkill(name string) *Skill {
	r := Skill{
		Value: name,
	}
	return &r
}

//createSkill
func (s *Skill) PostSkill(c *resty.Client, token string) error {
	body, err := json.Marshal(s)
	if err != nil {
		log.Printf("Error while Marshal: %v", err)
		return err
	}
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		Post(consts.UserServiceSkillUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("PostSkill CheckResponse failed")
	}
	err = json.Unmarshal(resp.Body(), s)
	if err != nil {
		log.Printf("Error while Unmarshal: %v", err)
		return err
	}

	return nil
}

//getAllSkills
func GetAllSkills(c *resty.Client, token string) (*[]Skill, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.UserServiceSkillUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("PostSkill CheckResponse failed")
	}

	skills := make([]Skill, 0)
	err = json.Unmarshal(resp.Body(), &skills)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return nil, err
	}

	return &skills, nil
}

//getSkillById
func (s *Skill) GetSkillById(c *resty.Client, token, id string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": id}).
		Get(consts.UserServiceSkillUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetAllSkills CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), s)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return err
	}
	return nil
}

//deleteSkillById
func (s *Skill) DeleteSkillById(c *resty.Client, token, id string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": id}).
		Delete(consts.UserServiceSkillUrl + "{id}")
	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteSkillById CheckResponse failed")
	}
	return nil
}

//Tag Skill
func (s *Skill) TagSkill(c *resty.Client, token, tags string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(tags).
		SetPathParams(map[string]string{"Id": s.Id}).
		Post(consts.UserServiceSkillUrl + "{Id}" + "/tags")

	if !helper.CheckResponse(resp, err) {
		return errors.New("TagGroup CheckResponse failed")
	}
	return nil
}

//updateSkill
func (s *Skill) PutSkill(c *resty.Client, token string) error {
	body, err := json.Marshal(s)
	if err != nil {
		log.Printf("Error while Marshal: %v", err)
		return err
	}
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": s.Id}).
		SetBody(body).
		Put(consts.UserServiceSkillUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("PutSkill CheckResponse failed")
	}
	err = json.Unmarshal(resp.Body(), s)
	if err != nil {
		log.Printf("Error while Unmarshal: %v", err)
		return err
	}

	return nil
}

func (s *Skill) PrintSkill() {
	fmt.Println("Printing Skill")
	fmt.Printf("Id: %v\n", s.Id)
	fmt.Printf("Value: %v\n", s.Value)
	fmt.Printf("Tags: %v\n", s.Tags)

	fmt.Println("-------------------------------------------------------------------------------------------------")
}

func PrintAllSkills(skills []Skill) {
	for _, s := range skills {
		s.PrintSkill()
	}
}
