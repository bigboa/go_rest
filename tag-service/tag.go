package tag_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
)

type Tag struct {
	Id    string `json:"id,omitempty"`
	Value string `json:"value,omitempty"`
}

type Tags []Tag

//!findAllTags
func GetAllTags(c *resty.Client, token string) (*Tags, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.TagServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAllTags CheckResponse failed")
	}

	res := make(Tags, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal GetAllTags: %v", err)
		return nil, err
	}
	return &res, nil
}

//!findById
func (t *Tag) GetTagById(c *resty.Client, token string) error {
	fmt.Printf("Tag Id to get: %v\n", t.Id)
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": t.Id}).
		Get(consts.TagServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetTagById CheckResponse failed")
	}
	err = json.Unmarshal(resp.Body(), t)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", string(resp.Body()))
		return err
	}
	return nil
}

//!createNewTag
func (t *Tags) CreateTag(c *resty.Client, token string) error {
	i := len(*t)
	body := "["
	for k, v := range *t {
		body += "\"" + v.Value + "\""
		if k < i-1 {
			body += ", "
		}
	}
	body += "]"
	fmt.Printf("Tag body: %v\n", string(body))
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		Post(consts.TagServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateTag CheckResponse failed")
	}
	err = json.Unmarshal(resp.Body(), t)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", string(resp.Body()))
		return err
	}
	return nil
}

//!deleteById
func (t *Tag) DeleteTagById(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": t.Id}).
		Delete(consts.TagServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetTagById CheckResponse failed")
	}

	return nil
}

func (t *Tag) Print() {
	fmt.Println("Printing Tag")
	fmt.Printf("ID: %v\n", t.Id)
	fmt.Printf("Value: %v\n", t.Value)
	fmt.Println("-----------------------------------------------------------------")
}
