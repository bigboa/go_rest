package test_cases

import (
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/user-service"
	"log"
	"math/rand"
	"strconv"
	"time"
)

func TestUser(c *resty.Client, t string) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())

	fmt.Println(consts.MsgHeader + "CreateUserAndGetToken is Started!")
	Email := "test_" + num + "@mail.io"
	FirstName := "FirstName_" + num
	LastName := "LastName_" + num
	MiddleName := "MiddleName_" + num
	Phone := num
	UserName := "UserName_" + num
	Password := "1234"
	uToken, err := user_service.CreateUserAndGetToken(Email, FirstName, LastName, MiddleName, Phone, UserName, Password)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "CreateUserAndGetToken is Done!")

	fmt.Println(consts.MsgHeader + "GetUserByToken is Started!")
	user := new(user_service.UserInfo)
	err = user.GetUserByToken(c, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	user.PrintUserInfo()
	fmt.Println(consts.MsgHeader + "GetUserByToken is Done!")

	fmt.Println(consts.MsgHeader + "TagUserByToken is Started!")
	err = user.TagUser(c, uToken.Token, "[\"TagUserByToken"+num+"\"]")
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "TagUserByToken is Done!")

	fmt.Println(consts.MsgHeader + "TagUserByAccountId is Started!")
	err = user.TagUserByAccId(c, t, "[\"TagUserByToken"+num+"\", \"TagUserByAccountId"+num+"\"]", user.AccountId)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "TagUserByAccountId is Done!")

	fmt.Println(consts.MsgHeader + "GetUserByAccountId is Started!")
	err = user.GetUserByAccountId(c, uToken.Token, user.AccountId)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	user.PrintUserInfo()
	fmt.Println(consts.MsgHeader + "GetUserByAccountId is Done!")

	fmt.Println(consts.MsgHeader + "CreateGroupInfo is Started!")
	gInfo := user_service.CreateGroupInfo(false, "GroupDescription_"+num, "GroupName_"+num, "ScreenName_"+num)
	err = gInfo.CreateGroup(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	gInfo.PrintGroup()
	gDel := user_service.CreateGroupInfo(false, "GroupDescription_ToDel_"+num, "GroupName_ToDel_"+num, "ScreenName_ToDel_"+num)
	err = gDel.CreateGroup(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	gDel.PrintGroup()
	fmt.Println(consts.MsgHeader + "CreateGroupInfo is Done!")

	fmt.Println(consts.MsgHeader + "AddGroupsToUserByAccountId is Started!")
	err = user.AddGroupsToUserByAccountId(c, t, user.AccountId, gInfo.Id)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "AddGroupsToUserByAccountId is Done!")

	fmt.Println(consts.MsgHeader + "AddGroupsToUserByToken is Started!")
	err = user.AddGroupsToUserByToken(c, uToken.Token, gDel.Id)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "AddGroupsToUserByToken is Done!")

	fmt.Println(consts.MsgHeader + "GetGroupsByAccountId is Started!")
	groups, err := user.GetGroupsByAccountId(c, t, user.AccountId)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, v := range *groups {
		v.PrintGroup()
	}
	fmt.Println(consts.MsgHeader + "GetGroupsByAccountId is Done!")

	fmt.Println(consts.MsgHeader + "DeleteUserFromGroupById is Started!")
	err = user.DeleteUserFromGroupById(c, uToken.Token, gDel.Id)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "DeleteUserFromGroupById is Done!")

	fmt.Println(consts.MsgHeader + "GetGroupsByToken is Started!")
	groups, err = user.GetGroupsByToken(c, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, v := range *groups {
		v.PrintGroup()
	}
	fmt.Println(consts.MsgHeader + "GetGroupsByToken is Done!")

	fmt.Println(consts.MsgHeader + "CreateSkill is Started!")
	skill := user_service.CreateSkill("Skill_" + num)
	err = skill.PostSkill(c, t)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	skillToDel := user_service.CreateSkill("SkillToDel_" + num)
	err = skillToDel.PostSkill(c, t)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "CreateSkill is Done!")

	fmt.Println(consts.MsgHeader + "AddSkillsToUserByAccountId is Started!")
	err = user.AddSkillsToUserByAccountId(c, t, user.AccountId, skill.Id)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "AddSkillsToUserByAccountId is Done!")

	fmt.Println(consts.MsgHeader + "AddSkillsToUserByToken is Started!")
	err = user.AddSkillsToUserByToken(c, uToken.Token, skillToDel.Id)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "AddSkillsToUserByToken is Done!")

	fmt.Println(consts.MsgHeader + "GetSkillsByToken is Started!")
	skills, err := user.GetSkillsByToken(c, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	for _, v := range *skills {
		v.PrintUserSkills()
	}
	fmt.Println(consts.MsgHeader + "GetSkillsByToken is Done!")

	fmt.Println(consts.MsgHeader + "DeleteUserSkillByToken is Started!")
	err = user.DeleteUserSkillByToken(c, uToken.Token, skillToDel.Id)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "DeleteUserSkillByToken is Done!")

	fmt.Println(consts.MsgHeader + "ConfirmSkill is Started!")
	err = user.ConfirmSkill(c, t, skill.Id, user.AccountId)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "ConfirmSkill is Done!")

	fmt.Println(consts.MsgHeader + "GetSkillsByToken is Started!")
	skills, err = user.GetSkillsByToken(c, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	for _, v := range *skills {
		v.PrintUserSkills()
	}
	fmt.Println(consts.MsgHeader + "GetSkillsByToken is Done!")

}
