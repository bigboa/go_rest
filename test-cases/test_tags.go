package test_cases

import (
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/tag-service"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

func TestTag(c *resty.Client, t string) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())

	allTags, err := tag_service.GetAllTags(c, t)
	if err != nil {
		log.Fatalf("Error during getting all tags: %v", err)
	}
	for _, tag := range *allTags {
		tag.Print()
	}

	tags := tag_service.Tags{
		tag_service.Tag{
			Value: "TestTag01_" + num,
		},
		tag_service.Tag{
			Value: "TestTag02_" + num,
		},
	}

	err = tags.CreateTag(c, t)
	if err != nil {
		log.Fatalf("Error : %v", err)
	}

	allTags, err = tag_service.GetAllTags(c, t)
	if err != nil {
		log.Fatalf("Error during getting all tags: %v", err)
	}
	id := ""
	for _, tag := range *allTags {
		tag.Print()
		if strings.Compare(tag.Value, "TestTag01_"+num) == 0 {
			id = tag.Id
		}
	}
	if len(id) > 0 {
		tag := new(tag_service.Tag)
		tag.Id = id
		err = tag.GetTagById(c, t)
		if err != nil {
			log.Fatalf("Error : %v", err)
		}
		tag.Print()
		err = tag.DeleteTagById(c, t)
		if err != nil {
			log.Fatalf("Error : %v", err)
		}
		allTags, err = tag_service.GetAllTags(c, t)
		if err != nil {
			log.Fatalf("Error during getting all tags: %v", err)
		}
		for _, tag := range *allTags {
			tag.Print()
		}

	}

}
