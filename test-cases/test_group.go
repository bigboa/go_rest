package test_cases

import (
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/user-service"
	"log"
	"math/rand"
	"strconv"
	"time"
)

func TestGroup(c *resty.Client, t string) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())

	Description := "GroupDescription_" + num
	GroupName := "GroupName_" + num
	ScreenName := "GroupScreenName_" + num

	gInfo := user_service.CreateGroupInfo(false, Description, GroupName, ScreenName)
	err := gInfo.CreateGroup(c, t)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "CreateGroupInfo is Done!")

	err = gInfo.TagGroup(c, t, "[\"TagGroup_"+num+"\"]")
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "TagGroup is Done!")

	err = gInfo.GetGroupById(c, t, gInfo.Id)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	gInfo.PrintGroup()
	fmt.Println(consts.MsgHeader + "GetGroupById is Done!")

	users, err := gInfo.GetUsersInGroupById(c, t, gInfo.Id)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + strconv.Itoa(len(*users)) + " users in group " + gInfo.GroupName)
	fmt.Println(consts.MsgHeader + "GetUsersInGroupById is Done!")

	err = gInfo.DeleteGroupById(c, t)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "DeleteGroupById is Done!")

	groups, err := user_service.GetAllGroups(c, t)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	user_service.PrintAllGroups(*groups)
	fmt.Println(consts.MsgHeader + "GetAllGroups is Done!")
}
