package test_cases

import (
	achievement_service "gitlab.com/NikolayZhurbin/go_rest/achievement-service"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/token"
	"log"
	"math/rand"
	"strconv"
	"time"
)

func TestAchievementType(r *token.AccessToken) {
	//get all achievement types
	achTypeList, err := achievement_service.GetAchievementTypeList(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error during getting all Achievement Types: %v\n", err)
	}
	//print all achievement types
	for _, achType := range *achTypeList {
		achType.Print()
	}
	//create new achievement type
	achType := achievement_service.AchievementType{
		Value: "testToDelete777",
	}
	err = achType.CreateAchievementType(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error while creating AchievementType: %v", err)
	}

	achType.Print()

	//delete achievement type
	err = achType.DeleteAchievementType(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error while DeleteAchievementType: %v", err)
	}

	//get all achievement types
	achTypeList, err = achievement_service.GetAchievementTypeList(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error during getting all Achievement Types: %v\n", err)
	}
	//print all achievement types
	for _, achType := range *achTypeList {
		achType.Print()
	}

}

func TestAchievement(r *token.AccessToken) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())

	allAch, err := achievement_service.GetAchievements(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	achievement_service.PrintAllAchievements(allAch)

	ach := achievement_service.Achievement{
		Description:       "Description_" + num,
		Name:              "Achievement_" + num,
		ParticipantsCount: 110,
		Date:              "2019-10-31",
	}
	err = ach.CreateAchievement(r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	ach.Print()

	err = ach.TagAchievement(consts.Client, r.Token, "[\"TagAchievement01\", \"TagAchievement02\"]")
	if err != nil {
		log.Fatalf("Error: %v", err)
	}

	c := achievement_service.Comment{
		Comment: "Achievement Comment_" + num,
	}
	err = c.CreateComment(consts.Client, r.Token, ach.Id)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	comments, err := achievement_service.GetCommentsById(consts.Client, r.Token, ach.Id)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	achievement_service.PrintAllComments(comments)

	ach.Name = ach.Name + "_Updated"
	err = ach.UpdateAchievement(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}

	allAch, err = achievement_service.GetAchievements(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	achievement_service.PrintAllAchievements(allAch)

	err = ach.DeleteAchievement(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}

	allAch, err = achievement_service.GetAchievements(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	achievement_service.PrintAllAchievements(allAch)
}
