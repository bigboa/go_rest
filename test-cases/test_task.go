package test_cases

import (
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/task-service"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

func TestPriority(c *resty.Client, t string) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())

	fmt.Println(consts.MsgHeader + "CreatePriority is Started!")
	p := task_service.Priority{Value: "Priority_" + num}
	err := p.CreatePriority(c, t)
	if err != nil {
		log.Fatalf("Error : %v", err)
	}
	fmt.Println(consts.MsgHeader + "CreatePriority is Done!")

	fmt.Println(consts.MsgHeader + "GetAllPriories is Started!")
	allP, err := task_service.GetAllPriories(c, t)
	if err != nil {
		log.Fatalf("Error : %v", err)
	}
	for _, pr := range *allP {
		pr.Print()
	}
	fmt.Println(consts.MsgHeader + "GetAllPriories is Done!")

	fmt.Println(consts.MsgHeader + "DeletePriority is Started!")
	err = p.DeletePriority(c, t)
	if err != nil {
		log.Fatalf("Error : %v", err)
	}
	fmt.Println(consts.MsgHeader + "DeletePriority is Done!")

	fmt.Println(consts.MsgHeader + "GetAllPriories is Started!")
	allP, err = task_service.GetAllPriories(c, t)
	if err != nil {
		log.Fatalf("Error : %v", err)
	}
	for _, pr := range *allP {
		pr.Print()
	}
	fmt.Println(consts.MsgHeader + "GetAllPriories is Done!")
}

func TestStatus(c *resty.Client, t string) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())

	fmt.Println(consts.MsgHeader + "CreateStatus is Started!")
	st := task_service.Status{Value: "Status_" + num}
	err := st.CreateStatus(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	st.Print()
	fmt.Println(consts.MsgHeader + "CreateStatus is Done!")

	fmt.Println(consts.MsgHeader + "GetAllStatuses is Started!")
	allSt, err := task_service.GetAllStatuses(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, st := range *allSt {
		st.Print()
	}
	fmt.Println(consts.MsgHeader + "GetAllStatuses is Done!")

	fmt.Println(consts.MsgHeader + "DeleteStatus is Started!")
	err = st.DeleteStatus(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "DeleteStatus is Done!")

	fmt.Println(consts.MsgHeader + "GetAllStatuses is Started!")
	allSt, err = task_service.GetAllStatuses(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, st := range *allSt {
		st.Print()
	}
	fmt.Println(consts.MsgHeader + "GetAllStatuses is Done!")
}

func TestTask(c *resty.Client, t, uId string) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())

	fmt.Println(consts.MsgHeader + "CreateComment is Started!")
	comment := task_service.Comment{
		AssignedTo: uId,
		DueDate:    "2020-12-31",
		Priority:   task_service.Priority{Id: "ed8906d0-ea68-11e9-8124-2e6b45b29a25"},
		Status:     task_service.Status{Id: "ed7dfcb8-ea68-11e9-8124-2e6b45b29a25"},
		Subject:    "TaskToDel_" + num,
		Text:       "TaskToDel_" + num,
	}
	err := comment.CreateComment(t)
	if err != nil {
		log.Printf("Error: %v", err)
	}
	str := []rune(comment.Id)
	comment.Id = string(str[1:37])
	fmt.Println(comment.Id)
	fmt.Println(consts.MsgHeader + "CreateComment is Done!")

	fmt.Println(consts.MsgHeader + "GetAllTasks is Started!")
	task := new(task_service.Task)
	allTasks, err := task_service.GetAllTasks(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, ts := range *allTasks {
		ts.Print()
		fmt.Println("Printing comments for task")
		fmt.Println(consts.MsgHeader + "GetAllCommentsByTaskId is Started!")
		allC, err := task_service.GetAllCommentsByTaskId(c, t, ts.Id)
		if err != nil {
			log.Fatalf("Error during getting all Comments: %v", err)
		}
		for _, c := range *allC {
			c.PrintComment()
			if strings.Compare(c.Id, comment.Id) == 0 {
				task.Id = c.TaskId
			}
		}
		fmt.Println(consts.MsgHeader + "GetAllCommentsByTaskId is Done!")
	}
	fmt.Println(consts.MsgHeader + "GetAllTasks is Done!")

	fmt.Println(consts.MsgHeader + "ReplyToComment is Started!")
	reply := task_service.Comment{
		AssignedTo: uId,
		DueDate:    "2020-12-31",
		Priority:   task_service.Priority{Id: "ed8906d0-ea68-11e9-8124-2e6b45b29a25"},
		Status:     task_service.Status{Id: "ed7dfcb8-ea68-11e9-8124-2e6b45b29a25"},
		Subject:    "ReplyTo_" + num,
		Text:       "ReplyTo_" + num,
	}
	err = reply.ReplyToComment(t, comment.Id, task.Id)
	if err != nil {
		log.Printf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "ReplyToComment is Done!")

	fmt.Println(consts.MsgHeader + "TagTask is Started!")
	err = task.TagTask(c, t, "[\"TagTask"+num+"\"]")
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "TagTask is Done!")

	fmt.Println(consts.MsgHeader + "DistributeTask is Started!")
	err = task.DistributeTask(c, t, &[]string{uId}, false)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "DistributeTask is Done!")

	fmt.Println(consts.MsgHeader + "GetAllTasks is Started!")
	allTasks, err = task_service.GetAllTasks(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, ts := range *allTasks {
		ts.Print()
		fmt.Println("Printing comments for task")
		fmt.Println(consts.MsgHeader + "GetAllCommentsByTaskId is Started!")
		allC, err := task_service.GetAllCommentsByTaskId(c, t, ts.Id)
		if err != nil {
			log.Fatalf("Error during getting all Comments: %v", err)
		}
		for _, c := range *allC {
			c.PrintComment()
			if strings.Compare(c.Id, comment.Id) == 0 {
				task.Id = c.TaskId
			}
		}
		fmt.Println(consts.MsgHeader + "GetAllCommentsByTaskId is Done!")
	}
	fmt.Println(consts.MsgHeader + "GetAllTasks is Done!")

	fmt.Println(consts.MsgHeader + "DeleteTask is Started!")
	err = task.DeleteTask(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "DeleteTask is Done!")

	fmt.Println(consts.MsgHeader + "GetAllTasks is Started!")
	allTasks, err = task_service.GetAllTasks(c, t)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, ts := range *allTasks {
		ts.Print()
	}
	fmt.Println(consts.MsgHeader + "GetAllTasks is Done!")

}
