package test_cases

import (
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/user-service"
	"log"
	"math/rand"
	"strconv"
	"time"
)

func TestSkill(c *resty.Client, t string) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())

	skill := user_service.CreateSkill("Skill_" + num)
	err := skill.PostSkill(c, t)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "CreateSkill is Done!")

	err = skill.GetSkillById(c, t, skill.Id)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "GetSkillById is Done!")
	skill.PrintSkill()

	err = skill.TagSkill(c, t, "[\"TagSkill_"+num+"\"]")
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "TagSkill is Done!")

	skill.Value = skill.Value + "_Updated"
	err = skill.PutSkill(c, t)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "PutSkill is Done!")
	skill.PrintSkill()

	err = skill.DeleteSkillById(c, t, skill.Id)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "DeleteSkill is Done!")

	skills, err := user_service.GetAllSkills(c, t)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
	fmt.Println(consts.MsgHeader + "GetAllSkills is Done!")
	user_service.PrintAllSkills(*skills)
}
