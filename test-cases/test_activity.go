package test_cases

import (
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/activity"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/token"
	"gitlab.com/NikolayZhurbin/go_rest/user-service"
	"log"
	"math/rand"
	"strconv"
	"time"
)

func TestActivity(c *resty.Client, r *token.AccessToken) {
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())
	fmt.Println(consts.MsgHeader + "CreateUserAndGetToken is Start!")
	uToken, err := user_service.CreateUserAndGetToken(
		"test"+num+"@test.ru",
		"First"+num+"Name",
		"Last"+num+"Name",
		"Middle"+num+"Name",
		"1234",
		"test"+num+"User",
		"1234",
	)
	num = strconv.Itoa(rNum.Int())
	uRoot, err := user_service.CreateUserAndGetToken(
		"test"+num+"@test.ru",
		"First"+num+"Name",
		"Last"+num+"Name",
		"Middle"+num+"Name",
		"1234",
		"test"+num+"User",
		"1234",
	)
	fmt.Println(consts.MsgHeader + "CreateUserAndGetToken is Done!")

	tags := "[\"TestActivity001" + num + "\", \"TestActivity002" + num + "\"]"

	fmt.Println(consts.MsgHeader + "CreateGroupWithTags is Start!")
	gInfo := user_service.CreateGroupWithTags(r, num, tags, true)
	fmt.Println("-GroupId: ", gInfo.Id)
	fmt.Println(consts.MsgHeader + "CreateGroupWithTags is Done!")

	fmt.Println(consts.MsgHeader + "GetUserByToken is Start!")
	uInfo := new(user_service.UserInfo)
	err = uInfo.GetUserByToken(consts.Client, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println("-User: ")
	uInfo.PrintUser()

	uInfoRoot := new(user_service.UserInfo)
	err = uInfoRoot.GetUserByToken(consts.Client, uRoot.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println("-User: ")
	uInfoRoot.PrintUser()
	fmt.Println(consts.MsgHeader + "GetUserByToken is Done!")

	fmt.Println(consts.MsgHeader + "AddGroupsToUserByAccountId is Start!")
	err = uInfo.AddGroupsToUserByAccountId(c, uRoot.Token, uInfo.AccountId, gInfo.Id)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "AddGroupsToUserByAccountId is Done!")

	fmt.Println(consts.MsgHeader + "CreateActivityWithTagToGroup is Start!")
	act := user_service.CreateActivityWithTagToGroup(uRoot, tags, gInfo.Id, time.Now().Format("2006-01-02 15:04"))
	act.PrintActivity(true)
	fmt.Println(consts.MsgHeader + "CreateActivityWithTagToGroup is Done!")

	fmt.Println(consts.MsgHeader + "ActivityCheckIn is Start!")
	err = act.ActivityCheckIn(c, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "ActivityCheckIn is Done!")

	fmt.Println(consts.MsgHeader + "ActivityStatuses is Start!")
	aStat, err := act.ActivityStatuses(c, uRoot.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, v := range *aStat {
		v.Print()
	}
	fmt.Println(consts.MsgHeader + "ActivityStatuses is Done!")

	fmt.Println(consts.MsgHeader + "GetTodayActivities is Start!")
	tAct, err := activity.GetTodayActivities(c, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, v := range *tAct {
		v.PrintActivity(true)
	}
	fmt.Println(consts.MsgHeader + "GetTodayActivities is Done!")

	fmt.Println(consts.MsgHeader + "GetAllActivities is Start!")
	tAct, err = activity.GetAllActivities(c, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, v := range *tAct {
		v.PrintActivity(true)
	}
	fmt.Println(consts.MsgHeader + "GetAllActivities is Done!")

	fmt.Println(consts.MsgHeader + "UpdateActivity is Start!")
	act.Description = act.Description + "_Updated"
	act.Subject = act.Subject + "_Updated"
	err = act.UpdateActivity(c, uRoot.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "UpdateActivity is Done!")

	fmt.Println(consts.MsgHeader + "GetActivitiesForPeriod is Start!")
	tAct, err = activity.GetActivitiesForPeriod(c, uToken.Token, time.Now().Add(-1*time.Hour*48).Format("2006-01-02"), time.Now().Add(time.Hour*48).Format("2006-01-02"))
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, v := range *tAct {
		v.PrintActivity(true)
	}
	fmt.Println(consts.MsgHeader + "GetActivitiesForPeriod is Done!")

	fmt.Println(consts.MsgHeader + "DeleteActivity is Start!")
	err = act.DeleteActivity(c, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println(consts.MsgHeader + "DeleteActivity is Done!")

	fmt.Println(consts.MsgHeader + "GetAllActivities is Start!")
	tAct, err = activity.GetAllActivities(c, uToken.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	for _, v := range *tAct {
		v.PrintActivity(true)
	}
	fmt.Println(consts.MsgHeader + "GetAllActivities is Done!")

}
